"use strict";

/* 1. Опишите своими словами, что такое метод обьекта:
Метод обьекта - эта некаая область (или переменная), которая имеет свои свойства и параметры, которые в теле обьекта. */

function createNewUser() {
  const newUser = {
    firstName: "",
    lastName: "",
    getLogin() {
      return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
    },
    setFirstName(value) {
      this.firstName = value;
    },
    setLastName(value) {
      if (value === null || value === "" || typeof value !== "string") {
        console.log("Error. Wrong last name!");
      } else {
        this.lastName = value;
      }
    },
  };

  do {
    newUser.setFirstName(prompt("Enter your first name:", newUser.firstName));
    newUser.setLastName(prompt("Enter your last name", newUser.lastName));
  } while (
    newUser.firstName === null ||
    newUser.firstName === "" ||
    typeof newUser.firstName !== "string" ||
    newUser.lastName === null ||
    newUser.lastName === "" ||
    typeof newUser.lastName !== "string"
  );

  return newUser;
}

const user1 = createNewUser();
console.log(user1.getLogin());
user1.firstName = "Vasya";
user1.lastName = "Pupkin";
console.log(user1.firstName);
console.log(user1.lastName);
