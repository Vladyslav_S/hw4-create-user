"use strict";

/* 1. Опишите своими словами, что такое метод обьекта:
Метод обьекта - эта некаая область (или переменная), которая имеет свои свойства и параметры, которые в теле обьекта. */

function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your first name:"),
    lastName: prompt("Enter your last name"),

    getLogin() {
      return (this._firstName.substring(0, 1) + this._lastName).toLowerCase();
    },
    setFirstName(value) {
      if (value === null || value === "" || typeof value !== "string") {
        console.log("Error. Wrong name!");
      } else {
        this._firstName = value;
      }
    },
    setLastName(value) {
      if (value === null || value === "" || typeof value !== "string") {
        console.log("Error. Wrong last name!");
      } else {
        this._lastName = value;
      }
    },
  };

  Object.defineProperty(newUser, "firstName", {
    // value: prompt("Enter your first name:"),
    writable: true,
    // configurable: false,
    // get: function () {
    //   return this._firstName;
    // },
    // set: function (value) {
    //   this._firstName = value;
    // },
  });
  Object.defineProperty(newUser, "lastName", {
    // value: prompt("Enter your last name:"),
    writable: false,
    // configurable: false,
    // get: function () {
    //   return this._lastName;
    // },
    // set: function (value) {
    //   this._lastName = value;
    // },
  });

  return newUser;
}

// const user1 = createNewUser();
// console.log(user1);
// // user1.setFirstName("Vasya");
// console.log(user1.firstName);
// // user1.setLastName("Mojsha");
// console.log(user1.lastName);
// // console.log(user1.getLogin());
// user1.firstName = "Masha";
// user1.lastName = "Tupaia";
// console.log(user1.firstName);
// console.log(user1.lastName);

const user1 = createNewUser();
console.log(user1);
user1.setFirstName("papapa");
console.log(user1);
